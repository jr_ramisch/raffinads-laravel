/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
global.Materialize = require('materialize-css');
import moment from 'moment';

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('example-component', require('./components/ExampleComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */


const app = new Vue({
    el: '#app',
    data: {
        nome: '',
        mostra : false,
        descricao : '',
        link : '',
        nome : '',
        data_criacao: '',
        caminho_imagem : '',
        jsonEdit : Object,
        dataAtual : new Date()

    },
    mounted() {
        if($("#adId").val())
        {
            let url = 'http://localhost:8000/anuncio/' + $("#adId").val() +'/edit';
            $.ajax({
                url : 'http://localhost:8000/anuncio/' + $("#adId").val() +'/edit',
                method : 'GET',
                data: {
                    id : $("#adId").val()
                },
                dataType : "JSON",
                success : function(data) {
                    this.nome = data.anuncio.nome;
                    $("#labelNome").addClass("active");
                    this.descricao = data.anuncio.descricao;
                    $("#labelDescricao").addClass("active");
                    this.link = data.anuncio.link;
                    $("#labelLink").addClass("active");
                    this.data_criacao = moment(data.anuncio.data_criacao).format('DD/MM/YYYY');
                    this.caminho_imagem = data.anuncio.caminho_imagem.slice(19);
                }.bind(this),
                fail : function(data) {
                    console.log("deu pau");
                }
            });
        }
    },
    methods : {
        showTemplate : function() {
            this.mostra = !this.mostra;
            this.data_criacao = moment(this.dataAtual.getFullYear()+'-'+this.dataAtual.getMonth()+'-'+this.dataAtual.getDate()).format('DD/MM/YYYY');
            this.getFields();
            this.loadImage();
        },
        validateAd : function() {

            this.inicio = $("#inicio").val();
            this.fim = $("#fim").val();

            let checkBoxes = $("input[type='checkbox']");
            let checkboxCounter = 0;

            for (let i = 0; i < checkBoxes.length; i++) {
                if (!checkBoxes[i].checked) {
                    checkboxCounter += 1;
                }
            }
            
            let showTime = 3000;
            let notChooseProduct = checkboxCounter == checkBoxes.length;
            let notFillAll = this.isFieldsEmpty();

            console.log(this.isFieldsEmpty());

            if (notChooseProduct) {
                this.triggerToast('Selecione pelo menos um produto', showTime);
                showTime += 2000;
            }

            if (notFillAll){
                this.triggerToast('Existem campos não preenchidos', showTime);
                showTime += 3000;
            }

            if (!notChooseProduct && !notFillAll) {
                $("#formulario-anuncio").submit();
            }
        },

        getFields : function() {
            this.caminho_imagem = $("#caminho_imagem").val();
            this.loadImage();
        },

        triggerToast : function(message, showTime) {
            Materialize.toast({html: message, displayLength : showTime, classes: 'rounded danger'});
        },

        loadImage : function() {
            
            let inputFile = $("#input-image")[0];
            
            if (inputFile.files[0]) {
                
                let reader  = new FileReader();

                reader.onload = function(e) {
                    var img = $('.imagem img').attr('src', e.target.result);
                    $('.imagem').html(img);
                }

                reader.readAsDataURL(inputFile.files[0]);
                
            }
        },
        isFieldsEmpty(){
            return (this.nome === '' || this.inicio === '' || this.fim === '' || this.link === '' || this.descricao === '')
        },
        editAdSuccess : function(data){
            console.log("chamou o success");
        },
        setModalId : function(event) {
            event.preventDefault();
            let formId = event.path[1].nextElementSibling.id;
            $("#idAnuncio").val(formId);
        },
        deleteAd: function () {
            let formId = '#'+$("#idAnuncio").val();
            $(formId).submit();
        },
        dashboardAlerts : function(element){
            console.log(element);
        }
    }
});

$(document).ready(function(){
    $('.modal').modal();

    $('select').formSelect({});

    $('.datepicker').datepicker({
        i18n : {
            today: 'Hoje',
            clear: 'Limpar',
            done: 'Ok',
            cancel: 'Cancelar',
            close: 'Fechar',
            nextMonth: 'Próximo mês',
            previousMonth: 'Mês anterior',
            weekdaysAbbrev: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S'],
            weekdaysShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb'],
            weekdays: ['Domingo', 'Segunda-Feira', 'Terça-Feira', 'Quarta-Feira', 'Quinta-Feira', 'Sexta-Feira', 'Sábado'],
            monthsShort: ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'],
            months: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],    
        },
        format: 'dd/mm/yyyy'
    });

    $('.tooltipped').tooltip({enterDelay : 20, exitDelay : 0});

    $('.dropdown-trigger').dropdown({
        inDuration: 510,
        outDuration: 400,
        constrain_width: false, 
        hover: true, 
        gutter: 0,
        coverTrigger: false,
        alignment: 'left' 
        }
    );
});

