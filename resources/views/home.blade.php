@extends('layouts.app')

@section('styles')
    <link rel="stylesheet" href="{{ asset('css/home.css') }}">
@endsection

@section('navbar')

    @include('layouts.navbar')

@endsection

@section('content')
<div class="main">
    <div class="container homeblade">
        <div class="row">
            <h5 class="titleProduto">Lista de Produtos</h5>
            <table class="white z-depth-3 responsive-table highlight">
                <thead>
                    <tr>
                        <th>Nome</th>
                        <th>Descrição</th>
                        <th>Editar</th>
                        <th>Ativo</th>
                    </tr>
                </thead>              
                <tbody class="highlight">
                    @foreach($produtos as $produto)    
                    <tr>
                        <td>{{ $produto -> nome}}</td>
                        <td>{{ $produto -> descricao}}</td>
                        <td><a href="produto/{{ $produto->id}}/edit"><i class="material-icons">create</i></a></td>
                        <td>
                        <p>
                            <label>
                                <input @if((int)$produto->ativo==1) {!! 'checked="checked" ' !!} @endif type="checkbox" class="orange darken-1 filled-in" disabled="disabled" name="ativo" 
                                 value="{{ $produto->ativo }}"/>
                                <span></span>
                            </label>
                         </p>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>    
        </div>
    </div>
</div>
@endsection
