@extends('layouts.app')

@section('styles')
    <link rel="stylesheet" href="{{ asset('css/dashboard.css') }}">
@endsection

@section('navbar')

    @include('layouts.navbar')

@endsection

@section('content')
<div class="container">
    <div class="row clearfix">
        @if(!$anuncios->isEmpty())
            @foreach($anuncios as $anuncio)
                <div class="col m4" style="margin-top:0.8%">
                    <div class="card medium">
                        <div class="card-image">
                            <img class="activator" src="{{ asset($anuncio->caminho_imagem) }}">
                        </div>

                        <div class="card-content">
                            <span class="card-title activator grey-text text-darken-4">{{ $anuncio->nome }}<i class="material-icons right">more_vert</i></span>
                            
                            <div class="col">
                                <a href="{{'https://' . $anuncio->link }}" target="_blank"> {{ $anuncio->nome }} </a>
                            </div>

                            <div class="col right mt-1">
                                <span class="new badge green right-align tooltipped" data-position="bottom" data-tooltip="Data de criação" data-badge-caption="{{ str_replace('-', '/', date('d-m-Y', strtotime($anuncio->data_criacao))) }}"></span>
                                @if(Auth::check())
                                    <span class="tooltipped" data-position="bottom" data-tooltip="Editar anúncio">
                                        <a href="{{ route('anuncio.show', $anuncio->id)}}">
                                            <i class="material-icons">create</i>            
                                        </a>
                                    </span>
                                @endif

                                @if(Auth::check())
                                    <span class="tooltipped" data-position="bottom" data-tooltip="Excluir Anúncio">
                                        <a href="#DesejaRemover" class="modal-trigger" href="#" v-on:click = "setModalId">
                                            <i class="material-icons right-align">delete</i>
                                        </a>
                                        <form id = "{{ $anuncio->id }}" action="{{ route('anuncio.destroy', $anuncio) }}" method="POST">
                                            @Method('DELETE')
                                            @CSRF
                                        </form>
                                    </span>
                                @endif
                            </div>

                        </div>

                        <div class="card-reveal">
                            <span class="card-title grey-text text-darken-4">{{ $anuncio->nome }}<i class="material-icons right">close</i></span>
                            <p>{{ $anuncio->descricao }}</p>
                        </div>
                    </div>
                </div>
            @endforeach
        @endif

        @if(!$anuncios->count())
            <div class="col m8 offset-m2">
                <div class="card small">
                    <div class="card-content center">
                        <p>Não há nenhum anúncio cadastrado.<p>
                    </div>  
                </div>
            </div>
        @endif
        <input type="hidden" value = "{{ redirectStatus }}" v-on:change="dashboardAlerts" />
    </div>
</div>

<div id="DesejaRemover" class="modal">
    <div class="modal-content">
        <h4 class="orange-text  text-darken-2">Deseja remover o anúncio?</h4>
        <p class="text-darken-2">Atenção! Ao remover o anúncio não será possível reutiliza-lo.</p>
    </div>
    <div class="modal-footer">
        <a href="#" class="modal-close btn btn-small waves-effect">Cancelar</a>
        <a href="#" class="btn btn-small red waves-effect" id="modalRemover" v-on:click="deleteAd">Remover</a>
    </div>
    <input type="hidden" value="" id="idAnuncio">
</div>

@endsection
