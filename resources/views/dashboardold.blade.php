@extends('layouts.app')

@section('styles')
    <link rel="stylesheet" href="{{ asset('css/dashboard.css') }}">
@endsection

@section('navbar')

    @include('layouts.navbar')

@endsection

@section('content')
        <div class="container">
            <div class="row">
            @if(!$anuncios1->isEmpty())
                @method('DELETE')
                <div class="col m6" style="margin-top:0.8%">
                    @foreach($anuncios1 as $anuncio1)
                        <div class="card horizontal">

                            <div class="imagem valign-wrapper">
                                <img src="{{ asset('storage/' . $anuncio1->caminho_imagem) }}">
                            </div>

                            <div class="anuncio">
                                <h6>{{ $anuncio1->nome }}</h6>

                                <div class="card-content anuncio-conteudo">
                                    <p class="flow-text">{{ $anuncio1->descricao }}</p>
                                </div>

                                <div class="anuncio-link">

                                    <div class="col" style="padding-left:0px;">
                                        <a class="tooltipped" data-position="bottom" data-tooltip="Visitar" target="_blank" href ="{{'http://' . $anuncio1->link }}"> {{ $anuncio1->nome }} </a>
                                    </div>
                                    
                                    <div class="col right">
                                        <span class="new badge red tooltipped" data-position="bottom" data-tooltip="Data final" data-badge-caption="{{ str_replace('-', '/', date('d-m-Y', strtotime($anuncio1->data_fim))) }}"></span>
                                        <span class="new badge green tooltipped" data-position="bottom" data-tooltip="Data inicial" data-badge-caption="{{ str_replace('-', '/', date('d-m-Y', strtotime($anuncio1->data_inicio))) }}"></span>
                                        @if(Auth::check() && !$trashed)
                                            <span class="tooltipped" data-position="bottom" data-tooltip="Editar anúncio">
                                                <a href="{{ route('anuncio.edit', $anuncio1->id)}}">
                                                    <i class="material-icons">create</i>            
                                                </a>
                                            </span>
                                        @endif
                            
                                        <span class="tooltipped" data-position="bottom" data-tooltip="Excluir Anúncio">
                                            @if(Auth::check() && !$trashed)
                                                <a href="#" v-on:click = "deleteAd">
                                                    <i class="material-icons right-align">delete</i>
                                                </a>
                                            @endif
                                            <form id = "{{ $anuncio1->id }}" action="{{ route('anuncio.destroy', $anuncio1) }}" method="POST">
                                                @Method('DELETE')
                                                @CSRF
                                            </form>
                                        </span>
                                    </div>
                                    
                                </div>
                            </div>  

                        </div>
                    @endforeach
                </div>
            @endif

            @if(!$anuncios2->isEmpty())
                <div class="col m6" style="margin-top: 0.8%;">
                    @foreach($anuncios2 as $anuncio2)
                        <div class="card horizontal">
                        
                            <div class="imagem valign-wrapper">
                                <img src="{{ asset('storage/' . $anuncio2->caminho_imagem) }}">
                            </div>

                            <div class="anuncio">
                                <h6>{{ $anuncio2->nome }}</h6>
                                <div class="card-content anuncio-conteudo">
                                    <p class="flow-text">{{ $anuncio2->descricao }}</p>
                                </div>

                                <div class="anuncio-link">
                                    <div class="col" style="padding-left:0px;">
                                        <a class="tooltipped" data-position="bottom" data-tooltip="Saiba mais" target="_blank" href="http://{{ $anuncio2->link }}"> {{ $anuncio2->nome }} </a>
                                    </div>
                                    
                                    <div class="col right">
                                        <span class="new badge red right-align tooltipped" data-position="bottom" data-tooltip="Data final" data-badge-caption="{{ str_replace('-', '/', date('d-m-Y', strtotime($anuncio1->data_fim))) }}"></span>
                                        <span class="new badge green right-align tooltipped" data-position="bottom" data-tooltip="Data inicial" data-badge-caption="{{ str_replace('-', '/', date('d-m-Y', strtotime($anuncio1->data_inicio))) }}"></span>
                                        @if(Auth::check() && !$trashed)
                                            <span class="tooltipped" data-position="bottom" data-tooltip="Editar anúncio">
                                                <a href="{{ route('anuncio.edit', $anuncio2->id)}}">
                                                    <i class="material-icons">create</i>            
                                                </a>
                                            </span>
                                        @endif

                                        @if(Auth::check() & !$trashed)
                                            <span class="tooltipped" data-position="bottom" data-tooltip="Excluir Anúncio">
                                                    <a href="#" v-on:click = "deleteAd">
                                                        <i class="material-icons right-align">delete</i>
                                                    </a>
                                                <form id = "{{ $anuncio2->id }}" action="{{ route('anuncio.destroy', $anuncio2) }}" method="POST">
                                                    @Method('DELETE')
                                                    @CSRF
                                                </form>
                                            </span>
                                        @endif
                                    </div>
                                    
                                </div>
                            </div>

                        </div>
                    @endforeach
                </div>
            @endif
            
            @if(!$anuncios1->count() && !$anuncios2->count())
                <div class="col m12">
                    <div class="card medium">
                        <div class="card-content">
                            <p>Não há nenhum anúncio cadastrado.<p>
                        </div>  
                    </div>
                </div>
            @endif
            </div>
         
        </div>
@endsection