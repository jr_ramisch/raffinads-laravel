<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <!-- Meta Info -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Raffinads</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
        <link rel="stylesheet" href="{{ asset('css/welcome.css') }}">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link rel="shortcut icon" href="https://raffinato.inf.br//img/site/980/favicon.png">
        
    </head>

    <body>
        <div class="flex-center position-ref full-height">

            <div class="content">
                <div class="title m-b-md">
                    Raffin<span>ads</span>
                </div>               
                <div>
                    <a class="waves-light btn waves-effect orange darken-1" href="{{ route('home') }}">
                        <i class="material-icons left white-text">view_module</i>Dashboard
                    </a>
                    <a class="waves-effect waves-light btn orange darken-1" href="{{ route('login') }}">
                        <i class="material-icons left">person</i>Login
                    </a>
                    <a class="waves-effect waves-light btn orange darken-1"href="{{ route('register') }}">
                        <i class="material-icons left">person_add</i>Registre-se
                    </a>
                </div>
            </div>
        </div>
    </body>
</html>
