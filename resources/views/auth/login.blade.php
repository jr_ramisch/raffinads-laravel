@extends('layouts.app')

@section('styles')
    <link rel="stylesheet" href="{{ asset('css/login.css') }}">
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col m6 offset-m3">
            <div class="card medium">
                <div class="card-content">

                    <h4 class="left">Login</h4>

                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="col m12">
                            <div class="input-field">
                                <input id="email" type="email" class="validade @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                <label for="email">E-mail</label>
                            </div>

                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col m12">
                            <div class="input-field">
                                <input id="password" type="password" class="validade @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                                <label for="password">Senha</label>
                            </div>

                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                            
                        </div>
                     
                        <div class="col m12">
                            <p>
                                <label>
                                    <input type="checkbox" class="filled-in" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}/>
                                    <span>Lembrar Senha</span>
                                </label>
                            </p>
                        </div>

                        <div class="col m12 login-btn center">
                            <button class="waves-effect waves-light btn orange darken-1" type="submit" name="action">Entrar</button>
                            @if (Route::has('password.request'))
                                <a class="waves-effect waves-light btn orange darken-1" href="{{ route('password.request') }}">Esqueceu sua Senha?</a>
                            @endif
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
