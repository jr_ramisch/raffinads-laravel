@section('navbar')
    <nav class="orange darken-1 shadow-xm">
        <div class="nav-wrapper">
            <div class="row">
                <div class="col m1 offset-m1">
                    <a class="navbar-brand logo" href="#">
                            Raffin<span class="ad">ads</span>
                    </a>
                </div>
                @guest
                    <div class="col m3">
                        <ul>
                            <li>
                                <a class="navbar-brand" href="{{ url('/') }}">
                                    Voltar
                                </a>
                            </li>
                        </ul>
                    </div>
                @else
                    <div class="col m4 valign-wrapper">
                        <ul>
                            <li>
                                <a href="{{ route('anuncio.index') }}">
                                Anúncios
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('anuncio.create') }}">
                                    Cadastrar Anúncio
                                </a>
                            </li>                                                                                
                        </ul>
                    </div>
                    
                    <div class="col m2 offset-m3">
                        <span>
                            Olá, {{ Auth::user()->name }}
                        </span>
                    </div>

                    <div class="col m1">
                        <a class="navbar-brand" href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                                            document.getElementById('logout-form').submit();">
                            Sair
                        </a>
                    </div>
                
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                @endguest
                
            </div>
        </div>
    </nav>
    
@show