@extends('layouts.app')

@section('styles')
    <link rel="stylesheet" href="{{ asset('css/anuncio.css') }}">
@endsection

@section('navbar')

    @include('layouts.navbar')

@endsection

@section('content')

<div class="container">  
    <div id="ad">
        <div class="row">

            <div class="col m12">

                <div class="card large">

                    <div class="card-content">
                        <h5>
                            Editar Anúncio
                        </h5>

                        <form action = "{{ route('anuncio.update', $id) }}" method="POST" enctype="multipart/form-data" id ="formulario-anuncio">
                            @csrf
                            @method('PUT')
                            <div class="col m6">

                                <div class="input-field col m12">
                                    <input name="nome" type="text" class="validate" id="nome" value="" v-model="nome">
                                    <label for="nome" id="labelNome">Nome</label>
                                </div>

                                <div class="input-field col m12">
                                    <textarea class="materialize-textarea" name="descricao" id="descricao" v-model="descricao"></textarea>
                                    <label for="textarea1" id="labelDescricao">Descrição do anúncio</label>
                                </div>

                                <div class="input-field col m12">
                                    <input name="link" type="text" value="" v-model="link">
                                    <label id="labelLink" for="link">Link do produto</label>
                                </div>

                            </div>

                            <div class="col m6"> 

                                <div class="file-field input-field col m12">
                                    <div class="btn btn-small orange darken-1 shadow-xm">
                                        <span>Imagem</span>
                                        <input type="file" name="imagem_produto" id="input-image">
                                    </div>

                                    <div class="file-path-wrapper">
                                        <input class="file-path validate" type="text" name="caminho_imagem" id="caminho_imagem" value="" placeholder="Escolha a imagem do produto" v-model="caminho_imagem">
                                    </div>
                                </div>

                                <div class="input-field col s12">
                                    @foreach($estados as $estado)
                                        <div class="col m3">
                                            <p>
                                                <label class="text-orange">
                                                    <input type="checkbox" checked="checked"/>
                                                    <span> {{ $estado }} </span>
                                                </label>
                                            </p>
                                        </div>
                                    @endforeach
                                </div>

                            </div>

                            <div class="col m12 center form-btn">
                                <a v-on:click="showTemplate" class="btn-small orange darken-1"> Pré-visualizar anúncio </a>
                                <a v-on:click="validateAd" class="waves-light btn btn-small orange darken-1"> Cadastrar </a>
                            </div>

                        </form>

                        <div class="col m4 offset-m4" v-if="mostra">
                            
                            <div class="card small">
                                <div class="card-image imagem">
                                    <img class="activator" src="{{ asset($caminho_imagem) }}">
                                </div>

                                <div class="card-content">
                                    <span class="card-title activator grey-text text-darken-4">@{{ nome }}<i class="material-icons right">more_vert</i></span>

                                    <div class="col mt-1">
                                        <a v-bind:href=" 'https://' + link" target="_blank"> @{{ nome }} </a>
                                    </div>

                                    <div class="col right mt-1">
                                        <span class="new badge green right-align tooltipped" data-position="bottom" data-tooltip="Data inicial" v-bind:data-badge-caption="data_criacao"></span>
                                    </div>
                                </div>

                                <div class="card-reveal">
                                    <span class="card-title grey-text text-darken-4">@{{ nome }}<i class="material-icons right">close</i></span>
                                    <p> @{{ descricao }} </p>
                                </div>
                            </div>

                        </div>

                    </div>
                    <input type="hidden" value="{{ $id }}" id="adId">
                </div>
            </div>
        </div>
    </div>
</div>

@endsection