<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CriarTabelaAnuncio extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Anuncios', function(Blueprint $table){
            $table->bigIncrements('id');
            $table->string('nome');
            $table->string('descricao')->nullable();;
            $table->date('data_inicio');
            $table->date('data_fim');
            $table->string('caminho_imagem');
            $table->string('link');
            $table->timestamps(); // timestamps adiciona os campos created_at e updated_at
            $table->softDeletes(); // softdeletes adiciona o campo deleted_at
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Anuncios');
    }
}
