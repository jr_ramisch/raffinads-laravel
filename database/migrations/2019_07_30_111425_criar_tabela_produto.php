<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CriarTabelaProduto extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Produtos', function(Blueprint $table){
            $table->bigIncrements('id');
            $table->string('nome');
            $table->string('descricao')->nullable();;
            $table->boolean('ativo');
            $table->integer('id_user');
            $table->string('caminho_imagem');
            $table->timestamps(); // timestamps adiciona os campos created_at e updated_at
            $table->softDeletes(); // softdeletes adiciona o campo deleted_at
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Produtos');
    }
}
