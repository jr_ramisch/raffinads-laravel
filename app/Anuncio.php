<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Anuncio extends Model
{

    use SoftDeletes;

    protected $fillable = [
        'nome', 'descricao', 'caminho_imagem', 'link', 'uf', 'data_criacao'
    ];
}
