<?php

namespace App\Http\Controllers;

use App\Anuncio;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class AnuncioController extends Controller
{
    private static $estados = ["BA", "CE", "DF", "ES", "GO", "MA", "MT", "MS", "MG", "PA", "PB", 
                               "PR", "PE", "PI", "RJ", "RN", "RS", "RO", "RR", "SC", "SE", "TO"];
    private static $REGISTRATION_SUCCESS = 0;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return redirect()->route('home');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('anuncio', ['estados' => AnuncioController::$estados]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $produtos = $request->produtos;

        $imagem = $request->file('imagem_produto');

        if ($imagem == null)
        {
            $nome_imagem = "/img/logo.png";
        }
        else
        {
            $nome_imagem = time() . $request->caminho_imagem; /* time() so pra nao sobreescrever imagens com o mesmo nome */
            $imagem->storeAs('public', $nome_imagem);
            $nome_imagem = '/storage' . '/' . time() . $request->caminho_imagem;
        }        

        $data_inicio = str_replace("/", "-", $request->data_inicio);
        $data_fim = str_replace("/", "-", $request->data_fim);

        $anuncio = Anuncio::create([
            'nome' => $request->nome,
            'descricao' => $request->descricao,
            'data_inicio' => date('Y-m-d', strtotime($data_inicio)),
            'data_fim' => date('Y-m-d', strtotime($data_fim)),                         
            'caminho_imagem' => $nome_imagem,
            'link' => $request->link
        ]);
        
        session(['redirectStatus' => $REGISTRATION_SUCCESS]);

        return redirect()->route('home');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Anuncio $anuncio)
    {
        return view('editanuncio', ['id' => $anuncio->id, 'caminho_imagem' => $anuncio->caminho_imagem, 'estados' => AnuncioController::$estados]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return response()->json(['anuncio' => Anuncio::find($id)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request->hasFile('caminho_imagem')){
            $imagem = $request->file('imagem_produto');
            $nome_imagem = time() . $request->caminho_imagem; /* time() so pra nao sobreescrever imagens com o mesmo nome */
            $imagem->storeAs('public', $nome_imagem);
            $imagem->save();   
        }
        
        $data_inicio = str_replace("/", "-", $request->data_inicio);
        $data_fim = str_replace("/", "-", $request->data_fim);

        $flight = Anuncio::find($id);
        $flight->nome = $request->nome;
        $flight->descricao = $request->descricao;
        $flight->data_inicio = date('Y-m-d', strtotime($data_inicio));
        $flight->data_fim = date('Y-m-d', strtotime($data_fim));                   
        $flight->link = $request->link;
        $flight->save();
        
        return redirect()->route('anuncio.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Anuncio $anuncio)
    {
        $anuncio->delete();
        return redirect()->route('anuncio.index');
    }
}
