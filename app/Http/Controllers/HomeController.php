<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers;
use App\Anuncio;

class HomeController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $anuncios = Anuncio::all();
        $redirectStatus = session()->pull('redirectStatus');

        if (isset($redirectStatus)) {
            return view('dashboard', ['anuncios' => $anuncios, '$redirectStatus' => $redirectStatus]);
        }
        
        return view('dashboard', ['anuncios' => $anuncios]);
    }
}
