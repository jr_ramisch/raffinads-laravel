#Raffinads
   Raffinads é um painel administrativo desenvolvido por Isabela Da Campo e Junior Vitor Ramisch, 
cujo objetivo geral é estreitar a comunicação entre a Raffinato e seus clientes efetivos. 
O que justifica a necessidade dessa implementação é o fato de que atualmente existem
clientes que já usam determinada solução Raffinato, porém desconhecem de outros
produtos disponíveis, estes por sua vez atendem uma necessidade real do cliente, no entanto
acabam não sendo rentabilizados pela simples desinformação.

Raffinads entra nessa situação com o objetivo de gerenciar a criação de anúncios e disseminar os mesmos 
através das soluções que já estão em uso.

##Dependências
   - Xampp (https://www.apachefriends.org/download.html);
   - NodeJS (https://nodejs.org/en/download/);
   - Composer (http://getcomposer.org./download);
   - Git (https://git-scm.com/downloads);

##Instalação

Para executar os passos seguintes tenha certeza de que todas as dependências estejam instaladas apropriadamente.

1. Primeiramente clone o repositório da aplicação executando o comando: `git clone https://jr_ramisch@bitbucket.org/jr_ramisch/raffinads.git`

2. Segundamente execute o comando `composer install` (isso pode levar algum tempo) e em seguida `npm run dev`.

3. Feito isso acesse localhost/phpmyadmin e crie a base com o nome `raffinads` (para isso é preciso iniciar os serviços MySQL e Apache através do `XAMPP Control Panel`).

4. Após a criação da base é preciso modificar o arquivo .env para que a aplicação consiga autenticar-se com o serviço MySQL, assim defina os campos da seguinte forma:
      
      
      
         DB_DATABASE=raffinads
         
         DB_USERNAME=root
         
         DB_PASSWORD=
         
      
      
5. Com o banco de dados configurado execute o comando `php artisan migrate` para fazer a criação das tabelas.

6. Feita as migrations execute o comando `php artisan key:generate` para gerar a chave usada para a identificação da aplicação e autenticação das requisições.

7. Finalmente use o camando `php artisan storage:link` para gerar um link de acesso público para o diretório onde as imagens são salvas e ainda `php artisan serve` para iniciar o servidor local.

8. Por último acesse `localhost:8000` para usar o `Raffinads`
